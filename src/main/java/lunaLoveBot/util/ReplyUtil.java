package lunaLoveBot.util;

import java.util.Timer;
import java.util.TimerTask;

import lunaLoveBot.LunaLoveBot;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;

public class ReplyUtil {
    private ReplyUtil() {
    }

    /**
     * Replyします
     * @param replyTo Reply先
     * @param msg ツイート内容
     * @throws TwitterException 
     */
    public static void delayReply(long replyTo, String msg, long delay){
        final StatusUpdate staup = new StatusUpdate(msg);
        staup.setInReplyToStatusId(replyTo);

        Timer timer = new Timer("DelayReplyTimer");
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                try {
                    LunaLoveBot.twitter.updateStatus(staup);
                } catch (TwitterException e) {
                    System.out.println("TweetError: " + e.getErrorMessage());
                }
            }
        }, delay);
    }
}
