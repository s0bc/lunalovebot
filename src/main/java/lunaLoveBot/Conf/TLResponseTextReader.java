package lunaLoveBot.Conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lunaLoveBot.util.ConfigurationUtil;

public class TLResponseTextReader {
    /**
     * 同一のものはない
     */
    private List<String> rex = new ArrayList<String>();
    private List<String> tltxt = new ArrayList<String>();

    private File fi = new File("tl.txt");

    /**
     * tl.txt読み込み
     */
    public void load(){
        boolean chk = true;
        rex.clear();
        tltxt.clear();

        if (!fi.exists()){
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                fw = new FileWriter(fi);
                bw = new BufferedWriter(fw);

                //バッファに出力
                bw.write("");

                //書き込み
                bw.flush();
            } catch (IOException e) {
                System.out.println(fi.getName() + "書き込みエラー");
            }finally{
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        FileReader fr = null;
        BufferedReader br = null;

        try {
            fr = new FileReader(fi);
            br = new BufferedReader(fr);

            String rstr = "";
            while((rstr = br.readLine()) != null){
                if (ConfigurationUtil.isComment(rstr) == false){

                    String[] tmp = rstr.split(":");
                    if (!(tmp.length >= 2)){
                        continue;
                    }
                    rexAdd(tmp[0]);
                    tltxt.add(rstr);
                }
            }


        } catch (FileNotFoundException e) {
            System.out.println("tl.txtが見つかりません。");
            chk = false;
        } catch (IOException e) {
            System.out.println("tl.txt読み込みエラー");
            chk = false;
            e.printStackTrace();
        } finally{
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (chk){
            System.out.println("tl.txtを読み込みました。 項目数: " + tltxt.size());
        }

        //デバッグ用処理
        /*
		System.out.println("ReplyList");
		for(String s:replytxt){
			System.out.println(s);
		}
		System.out.println("rexList");
		for(String s:rex){
			System.out.println(s);
		}
         */
    }

    private void rexAdd(String rex){
        if (this.rex.contains(rex) == false){
            this.rex.add(rex);
        }
    }

    public List<String> getRexList() {
        return rex;
    }

    public List<String> getTLTextList() {
        return tltxt;
    }
}
