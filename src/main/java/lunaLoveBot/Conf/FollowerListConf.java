package lunaLoveBot.Conf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lunaLoveBot.util.ConfigurationUtil;

public class FollowerListConf {
    private List<Long> folloerID = new ArrayList<Long>();

    public void load(){
        boolean chk = true;
        folloerID.clear();

        FileReader fr = null;
        BufferedReader bu = null;

        try {
            File fi = new File("follower.txt");
            if (!fi.exists()){
                save();
            }

            fr = new FileReader(fi);
            bu = new BufferedReader(fr);


            String str = "";
            int lineNo = 1;
            while ((str = bu.readLine()) != null) {
                if (ConfigurationUtil.isComment(str) == false){
                    try {
                        folloerID.add(Long.valueOf(str));
                    } catch (NumberFormatException e) {
                        System.out.println("値が異常です! follower.txt Line:" + lineNo);
                    }
                }
                lineNo++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("follower.txtが見つかりません。");
            chk = false;
        } catch (IOException e) {
            System.out.println("follower.txt読み込みエラー");
            chk = false;
        }finally{
            try {
                bu.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (chk){
                System.out.println("follower.txtを読み込みました。　List数: " + folloerID.size());
            }
        }

    }

    public void save(){
        boolean chk = true;

        FileWriter fw = null;
        BufferedWriter bw = null;

        try {
            fw = new FileWriter("follower.txt");
            bw = new BufferedWriter(fw);

            String wstr = "";
            for (long s : folloerID) {
                wstr = wstr + String.valueOf(s) + "\n";
            }

            //バッファに出力
            bw.write(wstr);

            //書き込み
            bw.flush();
        } catch (IOException e) {
            System.out.println("follower.txt書き込みエラー");
            chk = false;
        }finally{
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (chk){
            System.out.println("follower.txtを保存しました。");
        }
    }

    public void rmsaveFList(long id){
        folloerID.remove(id);
        save();
    }

    public List<Long> getFolloerIDList() {
        return folloerID;
    }
}
