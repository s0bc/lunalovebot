package lunaLoveBot.Conf;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesConf {

    private Properties prop = new Properties();
    private File fi = new File("sys.properties");

    /**
     * プロパティロード
     * 必要なキーを読み込む
     */
    public void loadProp(){
        if (!fi.exists()){
            prop.put("ConsumerKey", "");
            prop.put("OAuth", "false");
            prop.put("token", "");
            prop.put("tokenSecret", "");
            prop.put("ConsumerSecret", "");
            prop.put("Timer", "20");
            prop.put("FBTimer", "60");

            saveProp();
        }

        InputStream inStream = null;
        try {
            inStream = new BufferedInputStream(
                    new FileInputStream(fi));
            prop.load(inStream);

            System.out.println("Systemプロパティを読み込みました。");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Propセーブ
     */
    public void saveProp(){
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(fi));

            prop.store(out, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * プロパティアクセス用
     * @return
     */
    public Properties getProp() {
        return prop;
    }

    public String getConsumerKey() {
        return prop.getProperty("ConsumerKey");
    }

    public String getConsumerSecret() {
        return prop.getProperty("ConsumerSecret");
    }
}
