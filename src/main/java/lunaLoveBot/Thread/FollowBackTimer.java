package lunaLoveBot.Thread;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import lunaLoveBot.LunaLoveBot;
import twitter4j.IDs;
import twitter4j.TwitterException;

public class FollowBackTimer extends TimerTask{

    @Override
    public void run() {
        //LunaLoveBot.getFLConf().getFolloerIDList()
        List<Long> list = new ArrayList<Long>();

        try {
            IDs ids = LunaLoveBot.twitter.getFollowersIDs(LunaLoveBot.MyUserID, -1L);
            long[] idsL = ids.getIDs();

            for (int i = 0;i < idsL.length;i++){
                list.add(idsL[i]);
            }

            //比較　新しい方にしか含まれていないものを抽出し、フォローする
            for (long l : list) {
                if (LunaLoveBot.getFLConf().getFolloerIDList().contains(l) == false){
                    LunaLoveBot.twitter.createFriendship(l);
                    LunaLoveBot.getFLConf().getFolloerIDList().add(l);
                }
            }

            LunaLoveBot.getFLConf().save();
        } catch (TwitterException e) {
            e.printStackTrace();
        }

    }

}
